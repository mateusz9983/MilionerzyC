import socket, sys, threading
import sqlite3


class ClientSendThread(threading.Thread):
    def __init__(self, clients, data, id):
        self.clients = clients
        self.data = data
        self.id = id

    def run(self):
        for c in self.clients:
            c[1].sendall('%s: %s' % (id, self.data))


class ClientThread(threading.Thread):
    def __init__(self, connection, id, server):
        threading.Thread.__init__(self)
        self.connection = connection
        self.id = id
        self.server = server

    def run(self):
        # obsluga odbierania i wysylania danych
        data = self.connection.recv(1024)
        connection = sqlite3.connect('zdbEliminacje.db')
        connection.text_factory = lambda x: unicode(x, "utf-8", "ignore")
        cursor = connection.cursor()
        ListaZapytan = [];
        print "data = " + data
        ListaZapytan = data.split(" ")
        recive = ""
        x = ListaZapytan[0]
        if len(ListaZapytan) > 1:
            id = ListaZapytan[1]
        if x == '1':
            print x
            for row in cursor.execute("select Tresc from Pytania WHERE id=(%d)"%(int(id))):
                print row
            if row !=" ":
                recive += str(row)
                recive=recive[3:len(recive)-3]
            print "do wyslania" + recive
        elif x == '2':
            print "2"
            for row in cursor.execute("select odpowiedzi, poprawnakolejnosc from Pytania WHERE id=(%d)"%(int(id))):
                print row
            recive += str(row[0])
            recive +="/"
            recive += str(row[1][1:len(row[1])])#[1][1:len(row[1])])
            print recive
            recive=recive[1:len(recive)]
            print "do wyslania" + recive
        self.connection.sendall(bytes(str(recive)))
        self.connection.close()
pass


class Server:
    # server_adress = null
    def __init__(self, ip, port):
        self.server_adress = (ip, port)

        self.list_client = []

    def run(self):
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.bind(self.server_adress)
            sock.listen(2)
            print 'Starting up TCP server on %s port %s' % self.server_adress
            id = 0
            while True:
                id += 1
                connection, client_address = sock.accept()
                self.list_client.append(connection)
                c = ClientThread(connection, id, self)
                c.start()

        except socket.error, e:
            # ...
            pass


if __name__ == '__main__':
    s = Server('127.0.0.1', 666)
    s.run()
