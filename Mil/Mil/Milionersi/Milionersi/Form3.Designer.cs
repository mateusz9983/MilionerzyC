﻿namespace Milionersi
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.btnTelefon = new System.Windows.Forms.Button();
            this.btnPublicznosc = new System.Windows.Forms.Button();
            this.btnPolNaPol = new System.Windows.Forms.Button();
            this.textBoxPytania = new System.Windows.Forms.TextBox();
            this.labelTELEFON = new System.Windows.Forms.Label();
            this.labelPUBLICZNOSC = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.textBoxOdp = new System.Windows.Forms.TextBox();
            this.btnOdpA = new System.Windows.Forms.Button();
            this.btnOdpB = new System.Windows.Forms.Button();
            this.btnOdpC = new System.Windows.Forms.Button();
            this.btnOdpD = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.btnKoniec = new System.Windows.Forms.Button();
            this.btnZatwierdz = new System.Windows.Forms.Button();
            this.textBoxGraszO = new System.Windows.Forms.TextBox();
            this.textBoxObecnaKwota = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnTelefon
            // 
            this.btnTelefon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnTelefon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTelefon.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnTelefon.Image = ((System.Drawing.Image)(resources.GetObject("btnTelefon.Image")));
            this.btnTelefon.Location = new System.Drawing.Point(32, 27);
            this.btnTelefon.Name = "btnTelefon";
            this.btnTelefon.Size = new System.Drawing.Size(75, 77);
            this.btnTelefon.TabIndex = 0;
            this.btnTelefon.UseVisualStyleBackColor = true;
            this.btnTelefon.Click += new System.EventHandler(this.btnTelefon_Click);
            // 
            // btnPublicznosc
            // 
            this.btnPublicznosc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPublicznosc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPublicznosc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPublicznosc.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnPublicznosc.Image = ((System.Drawing.Image)(resources.GetObject("btnPublicznosc.Image")));
            this.btnPublicznosc.Location = new System.Drawing.Point(157, 27);
            this.btnPublicznosc.Name = "btnPublicznosc";
            this.btnPublicznosc.Size = new System.Drawing.Size(75, 77);
            this.btnPublicznosc.TabIndex = 1;
            this.btnPublicznosc.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnPublicznosc.UseVisualStyleBackColor = true;
            this.btnPublicznosc.Click += new System.EventHandler(this.btnPublicznosc_Click);
            // 
            // btnPolNaPol
            // 
            this.btnPolNaPol.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPolNaPol.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPolNaPol.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnPolNaPol.Image = ((System.Drawing.Image)(resources.GetObject("btnPolNaPol.Image")));
            this.btnPolNaPol.Location = new System.Drawing.Point(270, 28);
            this.btnPolNaPol.Name = "btnPolNaPol";
            this.btnPolNaPol.Size = new System.Drawing.Size(76, 77);
            this.btnPolNaPol.TabIndex = 2;
            this.btnPolNaPol.UseVisualStyleBackColor = true;
            this.btnPolNaPol.Click += new System.EventHandler(this.btnPolNaPol_Click);
            // 
            // textBoxPytania
            // 
            this.textBoxPytania.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxPytania.Location = new System.Drawing.Point(12, 163);
            this.textBoxPytania.Multiline = true;
            this.textBoxPytania.Name = "textBoxPytania";
            this.textBoxPytania.ReadOnly = true;
            this.textBoxPytania.Size = new System.Drawing.Size(680, 136);
            this.textBoxPytania.TabIndex = 3;
            // 
            // labelTELEFON
            // 
            this.labelTELEFON.AutoSize = true;
            this.labelTELEFON.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTELEFON.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelTELEFON.Location = new System.Drawing.Point(-1, 107);
            this.labelTELEFON.Name = "labelTELEFON";
            this.labelTELEFON.Size = new System.Drawing.Size(146, 18);
            this.labelTELEFON.TabIndex = 4;
            this.labelTELEFON.Text = "telefon do przyjaciela";
            // 
            // labelPUBLICZNOSC
            // 
            this.labelPUBLICZNOSC.AutoSize = true;
            this.labelPUBLICZNOSC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPUBLICZNOSC.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelPUBLICZNOSC.Location = new System.Drawing.Point(153, 107);
            this.labelPUBLICZNOSC.Name = "labelPUBLICZNOSC";
            this.labelPUBLICZNOSC.Size = new System.Drawing.Size(87, 18);
            this.labelPUBLICZNOSC.TabIndex = 5;
            this.labelPUBLICZNOSC.Text = "publiczność";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label50.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label50.Location = new System.Drawing.Point(287, 107);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(44, 18);
            this.label50.TabIndex = 6;
            this.label50.Text = "50/50";
            // 
            // textBoxOdp
            // 
            this.textBoxOdp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxOdp.Location = new System.Drawing.Point(12, 315);
            this.textBoxOdp.Multiline = true;
            this.textBoxOdp.Name = "textBoxOdp";
            this.textBoxOdp.ReadOnly = true;
            this.textBoxOdp.Size = new System.Drawing.Size(680, 118);
            this.textBoxOdp.TabIndex = 7;
            this.textBoxOdp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnOdpA
            // 
            this.btnOdpA.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnOdpA.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnOdpA.Location = new System.Drawing.Point(74, 441);
            this.btnOdpA.Name = "btnOdpA";
            this.btnOdpA.Size = new System.Drawing.Size(101, 32);
            this.btnOdpA.TabIndex = 8;
            this.btnOdpA.Text = "A";
            this.btnOdpA.UseVisualStyleBackColor = false;
            this.btnOdpA.Click += new System.EventHandler(this.btnOdpA_Click);
            // 
            // btnOdpB
            // 
            this.btnOdpB.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnOdpB.Location = new System.Drawing.Point(227, 442);
            this.btnOdpB.Name = "btnOdpB";
            this.btnOdpB.Size = new System.Drawing.Size(100, 32);
            this.btnOdpB.TabIndex = 9;
            this.btnOdpB.Text = "B";
            this.btnOdpB.UseVisualStyleBackColor = false;
            this.btnOdpB.Click += new System.EventHandler(this.btnOdpB_Click);
            // 
            // btnOdpC
            // 
            this.btnOdpC.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnOdpC.Location = new System.Drawing.Point(370, 442);
            this.btnOdpC.Name = "btnOdpC";
            this.btnOdpC.Size = new System.Drawing.Size(97, 32);
            this.btnOdpC.TabIndex = 10;
            this.btnOdpC.Text = "C";
            this.btnOdpC.UseVisualStyleBackColor = false;
            this.btnOdpC.Click += new System.EventHandler(this.btnOdpC_Click);
            // 
            // btnOdpD
            // 
            this.btnOdpD.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnOdpD.Location = new System.Drawing.Point(512, 442);
            this.btnOdpD.Name = "btnOdpD";
            this.btnOdpD.Size = new System.Drawing.Size(97, 31);
            this.btnOdpD.TabIndex = 11;
            this.btnOdpD.Text = "D";
            this.btnOdpD.UseVisualStyleBackColor = false;
            this.btnOdpD.Click += new System.EventHandler(this.btnOdpD_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.Location = new System.Drawing.Point(370, 13);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(129, 107);
            this.btnPlay.TabIndex = 12;
            this.btnPlay.Text = "GRAJ";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // btnKoniec
            // 
            this.btnKoniec.Location = new System.Drawing.Point(510, 68);
            this.btnKoniec.Name = "btnKoniec";
            this.btnKoniec.Size = new System.Drawing.Size(119, 52);
            this.btnKoniec.TabIndex = 13;
            this.btnKoniec.Text = "KONIEC";
            this.btnKoniec.UseVisualStyleBackColor = true;
            this.btnKoniec.Click += new System.EventHandler(this.btnKoniec_Click);
            // 
            // btnZatwierdz
            // 
            this.btnZatwierdz.Location = new System.Drawing.Point(510, 13);
            this.btnZatwierdz.Name = "btnZatwierdz";
            this.btnZatwierdz.Size = new System.Drawing.Size(119, 49);
            this.btnZatwierdz.TabIndex = 14;
            this.btnZatwierdz.Text = "ZATWIERDZ";
            this.btnZatwierdz.UseVisualStyleBackColor = true;
            this.btnZatwierdz.Click += new System.EventHandler(this.btnZatwierdz_Click);
            // 
            // textBoxGraszO
            // 
            this.textBoxGraszO.Location = new System.Drawing.Point(512, 137);
            this.textBoxGraszO.Name = "textBoxGraszO";
            this.textBoxGraszO.ReadOnly = true;
            this.textBoxGraszO.Size = new System.Drawing.Size(117, 20);
            this.textBoxGraszO.TabIndex = 15;
            // 
            // textBoxObecnaKwota
            // 
            this.textBoxObecnaKwota.Location = new System.Drawing.Point(198, 137);
            this.textBoxObecnaKwota.Name = "textBoxObecnaKwota";
            this.textBoxObecnaKwota.ReadOnly = true;
            this.textBoxObecnaKwota.Size = new System.Drawing.Size(129, 20);
            this.textBoxObecnaKwota.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(110, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Obecnie masz: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(450, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Grasz o: ";
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(704, 485);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxObecnaKwota);
            this.Controls.Add(this.textBoxGraszO);
            this.Controls.Add(this.btnZatwierdz);
            this.Controls.Add(this.btnKoniec);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.btnOdpD);
            this.Controls.Add(this.btnOdpC);
            this.Controls.Add(this.btnOdpB);
            this.Controls.Add(this.btnOdpA);
            this.Controls.Add(this.textBoxOdp);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.labelPUBLICZNOSC);
            this.Controls.Add(this.labelTELEFON);
            this.Controls.Add(this.textBoxPytania);
            this.Controls.Add(this.btnPolNaPol);
            this.Controls.Add(this.btnPublicznosc);
            this.Controls.Add(this.btnTelefon);
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gra Główna";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTelefon;
        private System.Windows.Forms.Button btnPublicznosc;
        private System.Windows.Forms.Button btnPolNaPol;
        private System.Windows.Forms.TextBox textBoxPytania;
        private System.Windows.Forms.Label labelTELEFON;
        private System.Windows.Forms.Label labelPUBLICZNOSC;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBoxOdp;
        private System.Windows.Forms.Button btnOdpA;
        private System.Windows.Forms.Button btnOdpB;
        private System.Windows.Forms.Button btnOdpC;
        private System.Windows.Forms.Button btnOdpD;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button btnKoniec;
        private System.Windows.Forms.Button btnZatwierdz;
        private System.Windows.Forms.TextBox textBoxGraszO;
        private System.Windows.Forms.TextBox textBoxObecnaKwota;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}