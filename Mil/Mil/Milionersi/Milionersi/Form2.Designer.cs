﻿namespace Milionersi
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.btnStart = new System.Windows.Forms.Button();
            this.textBoxPytania = new System.Windows.Forms.TextBox();
            this.textBoxOdpowiedzi = new System.Windows.Forms.TextBox();
            this.btnKoniec = new System.Windows.Forms.Button();
            this.textBoxCzas = new System.Windows.Forms.TextBox();
            this.labelCZAS = new System.Windows.Forms.Label();
            this.labelSTART = new System.Windows.Forms.Label();
            this.labelGOTOWE = new System.Windows.Forms.Label();
            this.btnD = new System.Windows.Forms.Button();
            this.btnC = new System.Windows.Forms.Button();
            this.btnB = new System.Windows.Forms.Button();
            this.btnA = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.Location = new System.Drawing.Point(12, 14);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(128, 97);
            this.btnStart.TabIndex = 0;
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // textBoxPytania
            // 
            this.textBoxPytania.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxPytania.Location = new System.Drawing.Point(11, 146);
            this.textBoxPytania.Multiline = true;
            this.textBoxPytania.Name = "textBoxPytania";
            this.textBoxPytania.ReadOnly = true;
            this.textBoxPytania.Size = new System.Drawing.Size(607, 127);
            this.textBoxPytania.TabIndex = 1;
            // 
            // textBoxOdpowiedzi
            // 
            this.textBoxOdpowiedzi.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxOdpowiedzi.Location = new System.Drawing.Point(12, 279);
            this.textBoxOdpowiedzi.Multiline = true;
            this.textBoxOdpowiedzi.Name = "textBoxOdpowiedzi";
            this.textBoxOdpowiedzi.ReadOnly = true;
            this.textBoxOdpowiedzi.Size = new System.Drawing.Size(607, 123);
            this.textBoxOdpowiedzi.TabIndex = 2;
            this.textBoxOdpowiedzi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnKoniec
            // 
            this.btnKoniec.Enabled = false;
            this.btnKoniec.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKoniec.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnKoniec.Image = ((System.Drawing.Image)(resources.GetObject("btnKoniec.Image")));
            this.btnKoniec.Location = new System.Drawing.Point(146, 14);
            this.btnKoniec.Name = "btnKoniec";
            this.btnKoniec.Size = new System.Drawing.Size(128, 97);
            this.btnKoniec.TabIndex = 7;
            this.btnKoniec.UseVisualStyleBackColor = true;
            this.btnKoniec.Click += new System.EventHandler(this.btnKoniec_Click);
            // 
            // textBoxCzas
            // 
            this.textBoxCzas.Location = new System.Drawing.Point(424, 64);
            this.textBoxCzas.Multiline = true;
            this.textBoxCzas.Name = "textBoxCzas";
            this.textBoxCzas.ReadOnly = true;
            this.textBoxCzas.Size = new System.Drawing.Size(131, 36);
            this.textBoxCzas.TabIndex = 8;
            // 
            // labelCZAS
            // 
            this.labelCZAS.AutoSize = true;
            this.labelCZAS.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCZAS.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelCZAS.Location = new System.Drawing.Point(280, 64);
            this.labelCZAS.Name = "labelCZAS";
            this.labelCZAS.Size = new System.Drawing.Size(138, 25);
            this.labelCZAS.TabIndex = 9;
            this.labelCZAS.Text = "Twój czas to:";
            // 
            // labelSTART
            // 
            this.labelSTART.AutoSize = true;
            this.labelSTART.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSTART.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelSTART.Location = new System.Drawing.Point(30, 114);
            this.labelSTART.Name = "labelSTART";
            this.labelSTART.Size = new System.Drawing.Size(81, 25);
            this.labelSTART.TabIndex = 10;
            this.labelSTART.Text = "START";
            // 
            // labelGOTOWE
            // 
            this.labelGOTOWE.AutoSize = true;
            this.labelGOTOWE.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelGOTOWE.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelGOTOWE.Location = new System.Drawing.Point(156, 114);
            this.labelGOTOWE.Name = "labelGOTOWE";
            this.labelGOTOWE.Size = new System.Drawing.Size(107, 25);
            this.labelGOTOWE.TabIndex = 11;
            this.labelGOTOWE.Text = "GOTOWE";
            // 
            // btnD
            // 
            this.btnD.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnD.Location = new System.Drawing.Point(483, 409);
            this.btnD.Name = "btnD";
            this.btnD.Size = new System.Drawing.Size(97, 31);
            this.btnD.TabIndex = 13;
            this.btnD.Text = "D";
            this.btnD.UseVisualStyleBackColor = false;
            this.btnD.Click += new System.EventHandler(this.btnD_Click);
            // 
            // btnC
            // 
            this.btnC.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnC.Location = new System.Drawing.Point(344, 408);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(97, 32);
            this.btnC.TabIndex = 16;
            this.btnC.Text = "C";
            this.btnC.UseVisualStyleBackColor = false;
            this.btnC.Click += new System.EventHandler(this.btnC_Click);
            // 
            // btnB
            // 
            this.btnB.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnB.Location = new System.Drawing.Point(207, 408);
            this.btnB.Name = "btnB";
            this.btnB.Size = new System.Drawing.Size(100, 32);
            this.btnB.TabIndex = 15;
            this.btnB.Text = "B";
            this.btnB.UseVisualStyleBackColor = false;
            this.btnB.Click += new System.EventHandler(this.btnB_Click);
            // 
            // btnA
            // 
            this.btnA.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnA.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnA.Location = new System.Drawing.Point(63, 408);
            this.btnA.Name = "btnA";
            this.btnA.Size = new System.Drawing.Size(101, 32);
            this.btnA.TabIndex = 14;
            this.btnA.Text = "A";
            this.btnA.UseVisualStyleBackColor = false;
            this.btnA.Click += new System.EventHandler(this.btnA_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(630, 456);
            this.Controls.Add(this.btnC);
            this.Controls.Add(this.btnB);
            this.Controls.Add(this.btnA);
            this.Controls.Add(this.btnD);
            this.Controls.Add(this.labelGOTOWE);
            this.Controls.Add(this.labelSTART);
            this.Controls.Add(this.labelCZAS);
            this.Controls.Add(this.textBoxCzas);
            this.Controls.Add(this.btnKoniec);
            this.Controls.Add(this.textBoxOdpowiedzi);
            this.Controls.Add(this.textBoxPytania);
            this.Controls.Add(this.btnStart);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Eliminacje";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox textBoxPytania;
        private System.Windows.Forms.TextBox textBoxOdpowiedzi;
        private System.Windows.Forms.Button btnKoniec;
        private System.Windows.Forms.TextBox textBoxCzas;
        private System.Windows.Forms.Label labelCZAS;
        private System.Windows.Forms.Label labelSTART;
        private System.Windows.Forms.Label labelGOTOWE;
        private System.Windows.Forms.Button btnD;
        private System.Windows.Forms.Button btnC;
        private System.Windows.Forms.Button btnB;
        private System.Windows.Forms.Button btnA;
    }
}