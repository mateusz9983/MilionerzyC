﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Milionersi
{
    public partial class Form2 : Form
    {
        
        public Form2()
        {
            InitializeComponent();
            WylaczPrzyciskiABCD();
        }


        string KolejnoscOdp="",PoprawnaKolejnosc="";
        DateTime start;
        DateTime stop;
        public TcpClient PolaczZserverem()
        {
            try
            {
                TcpClient client = new TcpClient("127.0.0.1", 666);
                return client;
            }
            catch
            {

                DialogResult dialogResultFAil = MessageBox.Show("Nie można połączyć z serwerem", "Coś poszło nie tak");
                koniec();
                return null;
            }
        }

        public void koniec()
        {
            Milionerzy startowa = new Milionerzy(); //przejscie do startowej
            startowa.Show();
            this.Hide();
        }
        public string pobierzPytanie(int numer) //pobieranie pytania
        {
            try
            {
                TcpClient client = PolaczZserverem();
                string message = "1 ";
                message += numer.ToString();
                Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
                client.GetStream().Write(data, 0, data.Length);
                data = new Byte[256];
                String responseData = String.Empty;
                Int32 bytes = client.GetStream().Read(data, 0, data.Length);
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                return responseData;
            }
            catch
            {
                return null;
            }
           
        }
        public string pobierzodp(int numer) //pobieranie odpowiedzi
        {
            try
            {
                TcpClient client = PolaczZserverem();
                string message = "2 ";
                message += numer.ToString();
                Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
                client.GetStream().Write(data, 0, data.Length);
                data = new Byte[256];
                String responseData = String.Empty;
                Int32 bytes = client.GetStream().Read(data, 0, data.Length);
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                return responseData;
            }
            catch
            {
                return null;
            }

        }

        public int losuj_numer_pytania()//losuj pytani
        {

            Random rnd = new Random();
            return rnd.Next(0, 39);
         }

        public void pokazPrzyciskiOdpowiedzi() //aktywacja przyciskow odp
        {
            btnA.Enabled = true;
            btnB.Enabled = true;
            btnC.Enabled = true;
            btnD.Enabled = true;
        }
        private void btnStart_Click(object sender, EventArgs e) 
        {
            
            try
            {
                btnStart.Enabled = false;
                pokazPrzyciskiOdpowiedzi();
                KolejnoscOdp = "";
                int x = losuj_numer_pytania();
                btnKoniec.Enabled = true;
                start = DateTime.Now;
                textBoxPytania.Text = pobierzPytanie(x).ToString();
                string OdpiPop; // odpowiedzi oraz poprawna kolejnosc
                OdpiPop = pobierzodp(x).ToString();
                String value = OdpiPop;
                Char delimiter = '/';
                String[] substrings = value.Split(delimiter);

                textBoxOdpowiedzi.Text = substrings[0];
                PoprawnaKolejnosc = substrings[1].ToString(); // przypisanie poprawnej kolejnosci
                Console.WriteLine(PoprawnaKolejnosc);
            }
            catch(Exception e1)

            {
                Console.WriteLine(e1.ToString());

            }

        }
        private void WylaczPrzyciskiABCD()
        {
            btnA.Enabled = false;
            btnB.Enabled = false;
            btnC.Enabled = false;
            btnD.Enabled = false;
        }
        private void btnA_Click(object sender, EventArgs e) //odp a
        {
            KolejnoscOdp += "a";
            btnA.Enabled = false;
            btnA.BackColor = Color.Orange;
        }

        private void btnB_Click(object sender, EventArgs e) //odp b
        {
            KolejnoscOdp += "b";
            btnB.Enabled = false;
            btnB.BackColor = Color.Orange;
        }

        private void btnC_Click(object sender, EventArgs e) //odp c
        {
            KolejnoscOdp += "c";
            btnC.Enabled = false;
            btnC.BackColor = Color.Orange;
        }

        private void btnD_Click(object sender, EventArgs e) // odp d
        {
            KolejnoscOdp += "d";
            btnD.Enabled = false;
            btnD.BackColor = Color.Orange;
        }
        public Przeciwnik[] GenerujPrzeciwnikow()  //generowanie przeciwnikow
        {
            string[] Imiona=new string[5];
            string[] Czas = new string[5];
            string[] allLines = File.ReadAllLines(@"C:\\Users\\Matje\\Documents\\Visual Studio 2017\\Projects\\Milionersi\\Milionersi\\imiona.txt");
            //Console.WriteLine(allLines[rnd.Next(allLines.Length)]);

            var random = new Random((int)DateTime.Now.Ticks);
            
            for (int j = 0; j < 5; j++) {
                int randomValue = random.Next(1, allLines.Length);
                double czas = random.NextDouble()*40+20;
                czas = Math.Round(czas, 3);
                Imiona[j] = allLines[randomValue];
                Czas[j] = czas.ToString();
                Console.WriteLine(Imiona[j]);
            }
            Przeciwnik[] przeciwnicy= new Przeciwnik[5];
            for (int i = 0; i < 5; i++)
            {
                przeciwnicy[i] = new Przeciwnik(Imiona[i],Czas[i]);
                Console.WriteLine(przeciwnicy[i].imie + " " + przeciwnicy[i].czas);
            }
            return przeciwnicy;

        }
        private void btnGraj_Click(object sender, EventArgs e)
        {
            Form3 glownaGra = new Form3(); //przejscie do gry
            glownaGra.Show();
            this.Hide();
        }

        

        private void btnKoniec_Click(object sender, EventArgs e) // zatwierdzenie odpowiedzi i panelu eliminacji
        {
            stop = DateTime.Now;
            //Boolean czypierwszy = true;
            string value = (stop - start).ToString();
            Char delimiter = ':';
            string[] substrings = value.Split(delimiter);
            string tmp = substrings[2].ToString();
            tmp = tmp.Substring(0, 6);
            textBoxCzas.Text = tmp ;  //pole z wynikiem czasu
            Console.WriteLine(KolejnoscOdp);
            Console.WriteLine(PoprawnaKolejnosc);
            KolejnoscOdp += "\n";
            Przeciwnik[] przeciwnicy = GenerujPrzeciwnikow();
            String listaPrzeciwnikow = "";
            for (int i = 0; i < 5; i++)
            {
                listaPrzeciwnikow += przeciwnicy[i].imie;
                listaPrzeciwnikow += " ";
                listaPrzeciwnikow += przeciwnicy[i].czas;
                listaPrzeciwnikow += "\n";
            }
            listaPrzeciwnikow += "Ty" + " " + tmp;
            if (KolejnoscOdp.Equals(PoprawnaKolejnosc))
            {
                Console.WriteLine("poprawna kolejnosc");
                MessageBox.Show(listaPrzeciwnikow,
    "Wyniki Eliminacji", MessageBoxButtons.OK,
        MessageBoxIcon.Information);
                Form3 gra = new Form3();
                gra.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Niepoprawna Kolejność",
    "Informacja", MessageBoxButtons.OK,
        MessageBoxIcon.Information);
                Milionerzy gra = new Milionerzy();
                gra.Show();
                this.Hide();
            }

        }

       
    }
}
