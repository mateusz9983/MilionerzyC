﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milionersi
{
   
    class Wygrana
    {

        public Wygrana() { }

        public string ZwrocWygranaZlaOdp(int liczbapoprawnychodp) // zwraca wygrana przy blednej odpowiedzi
        {
            if (liczbapoprawnychodp >= 2 && liczbapoprawnychodp < 7)
                return "1000";
            else if (liczbapoprawnychodp >= 7)
                return "40000";
            else
                return "0";

        }
        public string ZwrocWygrana(int liczbapoprawnychodp) 
        {
            string[] tabZkasa = new string[12];
            if (liczbapoprawnychodp == 0)
                return "0";
            tabZkasa[0] = "500";
            tabZkasa[1] = "1000";
            tabZkasa[2] = "2000";
            tabZkasa[3] = "5000";
            tabZkasa[4] = "10000";
            tabZkasa[5] = "20000";
            tabZkasa[6] = "40000";
            tabZkasa[7] = "75000";
            tabZkasa[8] = "125000";
            tabZkasa[9] = "250000";
            tabZkasa[10] = "500000";
            tabZkasa[11] = "MILION";
            return tabZkasa[liczbapoprawnychodp-1];

        }




    }
}
