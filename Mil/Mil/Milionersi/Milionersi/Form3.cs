﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milionersi
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            
            InitializeComponent();
            btnPublicznosc.Enabled = false;
            btnTelefon.Enabled = false;
            btnPolNaPol.Enabled = false;
            btnZatwierdz.Enabled = false;
            ukryjPrzyciskiOdpowiedzi();
        }
        string WybranaOdp = "", PoprawnaOdp = "";
        int LiczbaPoprawnychOdp = 0;
        int licznikTabPytan=0;
        int[] NumeryPytan = new int[44];
        Wygrana wygrana = new Wygrana();

        public TcpClient PolaczZserverem(){
            try
            {
                TcpClient client = new TcpClient("127.0.0.1", 666);
                return client;
            }
            catch
            {

               DialogResult dialogResultFAil = MessageBox.Show("Nie można połączyć z serwerem", "Coś poszło nie tak");
               koniec();
               return null;  
            }

    }      
        public string pobierzPytanie(int numer)
        {
            try
            {
                TcpClient client = PolaczZserverem();
                string message = "1 ";
                message += numer.ToString();
                Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
                client.GetStream().Write(data, 0, data.Length);
                data = new Byte[256];
                String responseData = String.Empty; Int32 bytes = client.GetStream().Read(data, 0, data.Length);
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                return responseData;
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.ToString());
                return null;
            }

        }
        public string pobierzOdpowiedz(int numer)
        {
            TcpClient client = PolaczZserverem();
            string message = "2 ";
            message += numer.ToString();
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
            try
            {
                client.GetStream().Write(data, 0, data.Length);
                data = new Byte[256];
                String responseData = String.Empty;
                Int32 bytes = client.GetStream().Read(data, 0, data.Length);
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                return responseData;
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.ToString());
                return null;
            }

        }
        
        public void play() //pobieranie danych z bazy 
        {

            try
            {
                pokazPrzyciskiOdpowiedzi();
                WybranaOdp = "";
                btnZatwierdz.Enabled = true;
                int x = losuj_numer_pytania();
                textBoxPytania.Text = pobierzPytanie(x).ToString();
                string OdpiPop;
                OdpiPop = pobierzOdpowiedz(x).ToString();
                String value = OdpiPop;
                Char delimiter = '/';
                String[] substrings = value.Split(delimiter);
                textBoxOdp.Text = substrings[0];
                PoprawnaOdp = substrings[1].ToString();
            }
            catch(Exception e1)
            {
                Console.WriteLine(e1.ToString());
            }
               
    
        }
        private void btnKoniec_Click(object sender, EventArgs e)
        {
            DialogResult dialogResultW = MessageBox.Show("Twoja wygrana to :" + wygrana.ZwrocWygrana(LiczbaPoprawnychOdp), "Wygrana");
            koniec();
        }

        public void koniec()
        {
            Milionerzy startowa = new Milionerzy(); //przejscie do startowej
            startowa.Show();
            this.Hide();
        }


        public void pokazPrzyciskiOdpowiedzi() //aktywacja przycisków
        {
            btnOdpA.Enabled = true;
            btnOdpB.Enabled = true;
            btnOdpC.Enabled = true;
            btnOdpD.Enabled = true;
        }

        public void pokazTextOdpowiedzi() //aktywacja przycisków
        {
            btnOdpA.Text = "A";
            btnOdpB.Text = "B";
            btnOdpC.Text = "C";
            btnOdpD.Text = "D";
        }

        public void ukryjPrzyciskiOdpowiedzi() //ukrywanie przycisków
        {
            btnOdpA.Enabled = false;
            btnOdpB.Enabled = false;
            btnOdpC.Enabled = false;
            btnOdpD.Enabled = false;
        }

        public void czyscPrzyciski() //czyszczenie przycisków po wyborze
        {
            btnOdpA.BackColor = Color.White;
            btnOdpB.BackColor = Color.White;
            btnOdpC.BackColor = Color.White;
            btnOdpD.BackColor = Color.White;
        }
        public void wylPrzyciski()
        {
            btnZatwierdz.Enabled = false;
            btnPublicznosc.Enabled = false;
        }

        public int losuj_numer_pytania() //losowanie nr pytania
        {

            Random rnd = new Random();
            int tmp= rnd.Next(40, 84);
            if (licznikTabPytan == 0)
            {
                NumeryPytan[0] = tmp;
                licznikTabPytan++;
                return tmp;
            }
            for(int i = 0; i < licznikTabPytan; i++)
            {
                if (NumeryPytan[i] == tmp)
                {
                    tmp = rnd.Next(40, 84);
                    i = 0;
                }
            }
            NumeryPytan[licznikTabPytan] = tmp;
            licznikTabPytan++;
        return tmp;
        }
        private void btnOdpA_Click(object sender, EventArgs e)
        {
            WybranaOdp += "a";
            ukryjPrzyciskiOdpowiedzi();
            btnOdpA.BackColor = Color.Orange;
        }
        private void btnOdpB_Click(object sender, EventArgs e)
        {
            WybranaOdp += "b";
            ukryjPrzyciskiOdpowiedzi();
            btnOdpB.BackColor = Color.Orange;
        }
        private void btnOdpC_Click(object sender, EventArgs e)
        {
            WybranaOdp += "c";
            ukryjPrzyciskiOdpowiedzi();
            btnOdpC.BackColor = Color.Orange;
        }
        private void btnOdpD_Click(object sender, EventArgs e)
        {
            WybranaOdp += "d";
            ukryjPrzyciskiOdpowiedzi();
            btnOdpD.BackColor = Color.Orange;
        }

        private void btnZatwierdz_Click(object sender, EventArgs e) //zatwierdzanie odpowiedzi
        {
            pokazTextOdpowiedzi();
            WybranaOdp += "\n";
            Console.WriteLine(WybranaOdp);
            Console.WriteLine(PoprawnaOdp);
            if (WybranaOdp.Equals(PoprawnaOdp))
            {
                if (LiczbaPoprawnychOdp == 11)
                {
                    MessageBox.Show("Zostałeś Milionerem!!!","Gratulacje", MessageBoxButtons.OK,MessageBoxIcon.Information);
                    Milionerzy PoczatekGry = new Milionerzy(); //przejscie do eliminacji
                    PoczatekGry.Show();
                    //this.Hide();
                    this.Close();               
                }
                else
                {
                    DialogResult dialogResultT = MessageBox.Show("To jest poprawna odpowiedz ! Czy grasz dalej ?", "Brawo!", MessageBoxButtons.YesNo);
                    LiczbaPoprawnychOdp++;
                    if (dialogResultT == DialogResult.Yes)
                    {
                        ustawKwoty();
                        czyscPrzyciski();
                        play();
                    }
                    else if (dialogResultT == DialogResult.No)
                    {
                        DialogResult dialogResultW = MessageBox.Show("Twoja wygrana to :" + wygrana.ZwrocWygrana(LiczbaPoprawnychOdp), "Wygrana");
                        koniec();
                    }
                }
            }
            else
            {
                string tmp = wygrana.ZwrocWygranaZlaOdp(LiczbaPoprawnychOdp);
                DialogResult dialogResultF = MessageBox.Show("Niestety to zła odpowiedz " + "\n" + "Poprawna odpowiedź to " + PoprawnaOdp + "\n" + "Twoja wygrana to: " + tmp, "Ups :(");
                koniec();

            }
        }
        private void btnTelefon_Click(object sender, EventArgs e) //kolo ratunkowe telefon
        {
            btnTelefon.Image = System.Drawing.Image.FromFile(@"C:\Users\Matje\Documents\Visual Studio 2017\Projects\Mil\Mil\Milionersi\Milionersi\Ikony\phone_red.png");
            btnTelefon.Enabled = false;
            DialogResult dialogResultT = MessageBox.Show("Hmm myśle że to odpowiedz: " + PoprawnaOdp, "Telefon do przyjaciela");
        }

        private void btnPublicznosc_Click(object sender, EventArgs e) //kolo ratunkowe publicznosc
        {
            btnPublicznosc.Image = System.Drawing.Image.FromFile(@"C:\Users\Matje\Documents\Visual Studio 2017\Projects\Mil\Mil\Milionersi\Milionersi\Ikony\user_red.png");
            btnPublicznosc.Enabled = false;
            Random rnd = new Random();
            int pierwszelosowanie, drugielosowanie, trzecielosowanie, calosc;
            calosc = 100;
            pierwszelosowanie = rnd.Next(70, 100);
            //Console.WriteLine(pierwszelosowanie);
            calosc -= pierwszelosowanie;
            drugielosowanie = rnd.Next(0, calosc);
            //Console.WriteLine(drugielosowanie);
            calosc -= drugielosowanie;
            trzecielosowanie = rnd.Next(0, calosc);
            calosc -= trzecielosowanie;
            //Console.WriteLine(trzecielosowanie);
            //Console.WriteLine(calosc);
            Console.WriteLine(PoprawnaOdp);
            string tmpPoprawanOdp;
            tmpPoprawanOdp = PoprawnaOdp.Substring(0, PoprawnaOdp.Length - 1);
            Console.WriteLine(PoprawnaOdp);
            Publicznosc p1, p2, p3, p4;
            if (tmpPoprawanOdp == "a")
            {
                p1 = new Publicznosc("a", pierwszelosowanie);
                p2 = new Publicznosc("b", drugielosowanie);
                p3 = new Publicznosc("c", trzecielosowanie);
                p4 = new Publicznosc("d", calosc);

            }
            else if (tmpPoprawanOdp == "b")
            {
                p1 = new Publicznosc("b", pierwszelosowanie);
                p2 = new Publicznosc("a", drugielosowanie);
                p3 = new Publicznosc("c", trzecielosowanie);
                p4 = new Publicznosc("d", calosc);

            }
            else if (tmpPoprawanOdp == "c")
            {
                p1 = new Publicznosc("c", pierwszelosowanie);
                p2 = new Publicznosc("a", drugielosowanie);
                p3 = new Publicznosc("b", trzecielosowanie);
                p4 = new Publicznosc("d", calosc);

            }
            else
            {
                p1 = new Publicznosc("d", pierwszelosowanie);
                p2 = new Publicznosc("a", drugielosowanie);
                p3 = new Publicznosc("b", trzecielosowanie);
                p4 = new Publicznosc("c", calosc);

            }
            Console.WriteLine(p1.WyswietlcOdp() + p1.WyswietlcWart());
            Console.WriteLine(p2.WyswietlcOdp() + p2.WyswietlcWart());
            Console.WriteLine(p3.WyswietlcOdp() + p3.WyswietlcWart());
            Console.WriteLine(p4.WyswietlcOdp() + p4.WyswietlcWart());
            DialogResult dialogResultP = MessageBox.Show(p1.WyswietlcOdp()+ ": "+p1.WyswietlcWart()+"\n"+p2.WyswietlcOdp()+": "+p2.WyswietlcWart()+"\n"+p3.WyswietlcOdp()+": "+p3.WyswietlcWart()+"\n"+p4.WyswietlcOdp()+": "+p4.WyswietlcWart(), "Wyniki glosowania to :");
        }
        private void btnPolNaPol_Click(object sender, EventArgs e) //kolo ratunkowe 50/50
        {
            btnPolNaPol.Image = System.Drawing.Image.FromFile(@"C:\Users\Matje\Documents\Visual Studio 2017\Projects\Mil\Mil\Milionersi\Milionersi\Ikony\50-speed-limit-sign-red.png");
            btnPolNaPol.Enabled = false;
            string tmpPoprawanOdp = PoprawnaOdp.Substring(0, PoprawnaOdp.Length - 1);
            Console.WriteLine(PoprawnaOdp);
            if (tmpPoprawanOdp == "a")
            {
                btnOdpB.Enabled = false;
                btnOdpB.Text = " ";
                btnOdpC.Enabled = false;
                btnOdpC.Text = " ";
                DialogResult dialogResultP50 = MessageBox.Show("Komputer losowo odrzucił odpowiedz B i C","wyniki 50/50");

            }
            else if (tmpPoprawanOdp == "b")
            {
                btnOdpA.Enabled = false;
                btnOdpA.Text = " ";
                btnOdpD.Enabled = false;
                btnOdpD.Text = " ";
                DialogResult dialogResultP50 = MessageBox.Show("Komputer losowo odrzucił odpowiedz A i D", "wyniki 50/50");

            }
            else if (tmpPoprawanOdp == "c")
            {
                btnOdpD.Enabled = false;
                btnOdpD.Text = " ";
                btnOdpA.Enabled = false;
                btnOdpA.Text = " ";
                DialogResult dialogResultP50 = MessageBox.Show("Komputer losowo odrzucił odpowiedz A i D", "wyniki 50/50");

            }
            else
            {
                btnOdpC.Enabled = false;
                btnOdpC.Text = " ";
                btnOdpB.Enabled = false;
                btnOdpB.Text = " ";
                DialogResult dialogResultP50 = MessageBox.Show("Komputer losowo odrzucił odpowiedz B i C", "wyniki 50/50");

            }    
        }
        private void ustawKwoty()
        {
            textBoxObecnaKwota.Text = wygrana.ZwrocWygrana(LiczbaPoprawnychOdp);
            textBoxGraszO.Text = wygrana.ZwrocWygrana(LiczbaPoprawnychOdp + 1);
        }
        private void btnPlay_Click(object sender, EventArgs e) 
        {
            ustawKwoty();
            play();
            btnPlay.Enabled = false;
            btnPublicznosc.Enabled = true;
            btnTelefon.Enabled = true;
            btnPolNaPol.Enabled = true;
            btnZatwierdz.Enabled = true;
        }
    }
}
    

